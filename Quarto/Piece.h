#pragma once
#include<cstdint>
#include <ostream>
class Piece
{


public:
	enum class Body : int8_t//punem int8_t ca sa foloseasca mai putina memorie
	{
		Full,
		Hollow
	};
	
	enum class Color : int8_t
	{
		Light,
		Dark
	};

	enum class Height : int8_t
	{
		Short,
		Tall
	};
	
	enum class Shape : int8_t
	{
		Round,
		Square
	};
	Piece(Body body, Color color, Height height, Shape shape);

	Body GetBody() const;
	Color GetColor() const;//this este const
	Height GetHeight() const;
	Shape GetShape() const;

	~Piece();

	friend std::ostream& operator<<(std::ostream&,const Piece&);//cu friend poti accesa variabilele private
private:
	Body m_body : 1;//aloca un bit pentru data membra respectiva
	Color m_color : 1;
	Height m_height : 1;
	Shape m_shape : 1;//daca mai puneam un membru in enum shape,trebuia ca m_shape : 2 ca sa existe memorie
};

